require "bgsub"
local Connect = require "connect"
local Data = {}

function Data.getImageURL(name, filename)
    local body = Connect.getWikiPageBody(name)
    if not body then return nil end
    local imguri = body:match(([[src="(/images/./../%s)"]]):format(filename))
        or body:match(([[src="(/images/./../%s%%....)"]]):format(name))
    return imguri and Connect.WIKIMON .. imguri
end

local function cleanTextMarkup(text)
    if not text then return nil end
    return text
        :gsub("<br>", "\n")
        :gsub("<..-/>", "")
        :gsub("'''(.-)'''", "%1")
        :bgsub("%]%]", "%[%[", function(b)
            b = b:gsub("|w=", "|")
            return b:match("^%[%[.-|?([^|]*)%]%]$")
        end):bgsub("}}", "{{", function(b)
            b = b:gsub("|w=", "|")
            local tag, content = b:match("^{{(.-)|?([^|]*)}}$")
            if tag:match("xab") then
                return ("\nThe effect on %s's Digicore due to the X-Antibody\n")
                    :format(content)
            elseif tag:match("[Nn]ote") or tag:match("DD") then
                return ""
            elseif content:match("br") then
                return "\n"
            elseif tag:match("^EQ|") or tag:match("^AT|") then
                return tag:match("^.*|(.+)$")
            else
                return content
            end
        end):bgsub("</(%w+)>", "<%1%s*.->", function(b, tag)
            if tag == "span" then
                return b:match("%[%S* (.-)%]")
            else return "" end
        end)
end

function Data.parseDescription(text)
    local desc = text:match("|pe%w-=(.-)\n|") or text:match("|e%w-=(.-)\n|")
    return desc and cleanTextMarkup(desc)
end

function Data.parseImage(name, text)
    local imgfile = text:match("|image=(.-)\n")
    return imgfile and Data.getImageURL(name, (imgfile:gsub(" ", "_")))
end

function Data.parseInfoList(code, text)
    local list = {}
    for e in text:gmatch("|" .. code .. "%d+=([^\n][^\n]-)\n") do
        table.insert(list, e)
    end
    return #list > 0 and list or nil
end

function Data.parseWeight(text)
    local w = text:match("\n|w=(.-)\n")
    return w and w ~= "" and w .. "G" or nil
end

function Data.parseEvoList(text)
    local list = {}
    for evo in text:gmatch("%*.-%[%[(.-)%]%].-\n") do
        table.insert(list, evo)
    end
    return #list > 0 and list or nil
end

function Data.parseEvolutions(text)
    local evo = text:match("==Evolves [tT]o==(..-\n)=.-=\n") or ""
    return Data.parseEvoList(evo)
end

function Data.parseDevolutions(text)
    local devo = text:match("==Evolves [fF]rom==(.+)==Evolves To==") or ""
    return Data.parseEvoList(devo)
end

function Data.parseTechniques(text)
    local list = {}
    local techs = text:match("=Attack Techniques=\n%{%{T\n(.-)\n%}%}")
    techs = techs and techs .. "\n\n" or ""
    for t in techs:gmatch(".-\n\n") do
        local name = cleanTextMarkup(t:match("|name.-=(.-)\n"))
        local desc = cleanTextMarkup(t:match("|desc.-=(.-)\n"))
        if name then
            table.insert(list, {
                name = name,
                description = desc and cleanTextMarkup(desc) or ""
            })
        end
    end
    return #list > 0 and list or nil
end

function Data.getDigimon(name)
    local text, title = Connect.getWikiPageText(name)
    if not text then return nil end
    local digimon = {}
    digimon.name = title
    digimon.image = Data.parseImage(name, text)
    digimon.description = Data.parseDescription(text)
    digimon.type = Data.parseInfoList("t", text)
    digimon.level = Data.parseInfoList("l", text)
    digimon.attribute = Data.parseInfoList("a", text)
    digimon.fields = Data.parseInfoList("f", text)
    digimon.weight = Data.parseWeight(text)
    digimon.evolutions = Data.parseEvolutions(text)
    digimon.devolutions = Data.parseDevolutions(text)
    digimon.techniques = Data.parseTechniques(text)
    return digimon
end

return Data
