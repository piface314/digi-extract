local insert = table.insert
local remove = table.remove
local unpack = table.unpack or unpack

function string.gfind(s, pattern, plain)
    return coroutine.wrap(function()
        local st = 1
        while true do
            local find = { s:find(pattern, st, plain) }
            if not find[1] then break end
            local i, j, capt = find[1], find[2], {}
            for i = 3, #find do insert(capt, find[i]) end
            coroutine.yield(i, j, capt)
            st = j + 1
        end
    end)
end

local repfmt, reppat = "@R%d@", "@R(%d+)@"
local function aprep(text, reps)
    local function repf(i) return reps[tonumber(i)] end
    local n = #reps
    for i = 2, n do reps[i] = reps[i]:gsub(reppat, repf) end
    return text:gsub(reppat, repf)
end

local function nextb(text, cl, op, repl, reps)
    for ic, jc, capt in text:gfind(cl) do
        local op = op:gsub("%%(%d)", function(i) return capt[tonumber(i)] end)
        local find = { text:sub(1, ic - 1):find("^.*(" .. op .. ".-)$") }
        local fn = #find
        if fn > 0 then
            local io = ic - #find[3]
            for i = 4, fn do insert(capt, find[i]) end
            local b = text:sub(io, jc)
            local r = repl(b, unpack(capt)) or b
            insert(reps, r)
            return text:sub(1, io-1)..repfmt:format(#reps)..text:sub(jc+1), true
        end
    end
    return text, false
end

function string.bgsub(text, cl, op, repl)
    local reps = {}
    while true do
        local t, subs = nextb(text, cl, op, repl, reps)
        if not subs then break end
        text = t
    end
    return aprep(text, reps), #reps
end

return true
