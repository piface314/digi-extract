local http_request = require "http.request"
local http_headers = require "http.headers"
local json = require "json"

local Connect = {}

local WIKIMON = "https://wikimon.net"
local APIURL = "http://api.primary157.com.br:8000"
Connect.WIKIMON = WIKIMON

local hexchar = "%%%X"
local function sanitize(t)
    return (t:gsub(".", function(c) return hexchar:format(c:byte()) end))
end

function Connect.getWikiPageBody(page)
    local headers, stream = http_request.new_from_uri(WIKIMON .. "/" .. sanitize(page)):go(5)
    if not headers then return nil, stream end
    local body, msg = stream:get_body_as_string()
    if not body then return nil, msg end
    if headers:get(":status") ~= "200" then
        return nil, body
    end
    return body
end

function Connect.getWikiPageText(page)
    local headers, stream = http_request.new_from_uri(
        ("%s/api.php?action=parse&page=%s&prop=wikitext&format=json")
        :format(WIKIMON, sanitize(page))):go(15)
    if not headers then return nil, stream end
    local body, msg = stream:get_body_as_string()
    if not body then return nil, msg end
    if headers:get(":status") ~= "200" then
        return nil, body
    end
    local res = json.decode(body)
    if res.parse then
        local text = res.parse.wikitext["*"]
        local redirect = text:match([[^#REDIRECT %[%[(.+)%]%]$]])
        if redirect then
            return Connect.getWikiPageText(redirect:gsub(" ", "_"))
        else
            return text, res.parse.title
        end
    else
        return nil
    end
end

function Connect.postDigimon(digimon)
    local request = http_request.new_from_uri(APIURL .. "/digimons")
    request.headers:upsert(":method", "POST", false)
    request.headers:upsert("content-type", "application/json; charset=utf-8", false)
    request:set_body(json.encode(digimon))
    local headers, stream = request:go()
    if not headers then return false, stream end
    local body, msg = stream:get_body_as_string()
    if not body then return false, msg end
    return true, headers, body
end

return Connect
