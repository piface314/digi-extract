require "lua-modules.set-paths"

local function singleDigimon(name, post)
    local Data = require "data"
    local json = require "json"
    local digimon = Data.getDigimon(name)
    if post then
        local Connect = require 'connect'
        return Connect.postDigimon(digimon)
    else
        return json.encode(digimon)
    end
end

if arg[1] then
    print(singleDigimon(arg[1], arg[2]))
else
    local List = require "list"
    print("Extracting Digimons from `https://wikimon.net/Visual_List_of_Digimon`...")
    done, msg = List.postAll()
    print(done and "Done!" or msg)
end
