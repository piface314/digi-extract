local Connect = require "connect"
local Data = require "data"

local List = {}

function List.parse(text)
    local list = {}
    for entry in text:gmatch("{{DlistI(.-)}}") do
        local name = entry:match("|n=(.-)\n")
        if name then
            table.insert(list, name)
        end
    end
    return list
end

local barsize, dc, lc = 32, "#", " "
local function progbar(i, n, msg)
    local prog = i / n
    local done = math.floor(prog * barsize)
    io.write(("\r[\27[92m\27[1m%s%s\27[0m] %5.1f%% - %6d/%d%s%60s"):format(
        dc:rep(done), lc:rep(barsize - done),
        prog * 100, i, n, msg and " - " or "", msg or ""))
    io.output():flush()
end

function List.postAll()
    local text = Connect.getWikiPageText("Visual_List_of_Digimon")
    if not text then return false, "Unable to get list" end
    local list = List.parse(text)
    local n = #list
    for i, name in ipairs(list) do
        progbar(i, n, ("Fetching %q..."):format(name))
        local digimon = Data.getDigimon(name)
        if digimon then
            progbar(i, n, ("POSTing %q..."):format(digimon.name or ""))
            Connect.postDigimon(digimon)
        end
    end
    print()
    return true
end

return List
